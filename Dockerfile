FROM node:12.17.0

# Update apt, install dependencies
RUN apt update -y
RUN update-ca-certificates
RUN apt install g++ gcc make python3-pip python3-dev zip -y
RUN pip3 install awscli

# Install terraform
RUN wget https://releases.hashicorp.com/terraform/0.13.6/terraform_0.13.6_linux_amd64.zip
RUN unzip -o terraform_0.13.6_linux_amd64.zip -d /usr/local/bin/

# Install chrome
RUN wget -q -O - https://dl.google.com/linux/linux_signing_key.pub | apt-key add -
RUN echo "deb http://dl.google.com/linux/chrome/deb/ stable main" | tee /etc/apt/sources.list.d/google-chrome.list
RUN apt-get update -y
RUN apt install -y google-chrome-stable

# Install pm2 and serve
RUN npm install -g serve
RUN npm install -g pm2